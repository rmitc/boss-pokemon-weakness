package com.rmitc.pokemon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@EnableCaching
@SpringBootApplication
public class BossPokemonWeaknessApplication {

	public static void main(String[] args) {
		SpringApplication.run(BossPokemonWeaknessApplication.class, args);
	}

}
