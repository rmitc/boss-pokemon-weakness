package com.rmitc.pokemon.boss;

import static com.rmitc.pokemon.cache.Caches.CACHE_KEY_BOSS_POKEMON;
import static com.rmitc.pokemon.cache.Caches.CACHE_KEY_POKEMON_TYPE_DAMAGE;

import com.rmitc.pokemon.boss.dto.PokemonDto;
import com.rmitc.pokemon.boss.dto.PokemonTypeDamageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/pokemon/boss")
public class PokemonResource {

    @Autowired
    private PokemonService pokemonService;

    @GetMapping("{name}")
    @Cacheable(CACHE_KEY_BOSS_POKEMON)
    public PokemonDto getPokemon(@PathVariable String name) {
        return pokemonService.getPokemon(name);
    }

    @GetMapping("type/{type}")
    @Cacheable(CACHE_KEY_POKEMON_TYPE_DAMAGE)
    public PokemonTypeDamageDto pokemonTypeDamage(@PathVariable String type) {
        return pokemonService.getPokemonTypeDamage(type);
    }

}
