package com.rmitc.pokemon.boss;

import static com.rmitc.pokemon.model.PokemonTypeEffectiveness.Effective;
import static com.rmitc.pokemon.model.PokemonTypeEffectiveness.Immune;
import static com.rmitc.pokemon.model.PokemonTypeEffectiveness.Ineffective;
import static com.rmitc.pokemon.model.PokemonTypeEffectiveness.Neutral;
import static java.util.Optional.ofNullable;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.rmitc.pokemon.boss.dto.PokemonDto;
import com.rmitc.pokemon.boss.dto.PokemonTypeDamageDto;
import com.rmitc.pokemon.boss.exception.PokemonServiceException;
import com.rmitc.pokemon.client.PokeApiClient;
import com.rmitc.pokemon.model.PokemonTypeEffectiveness;
import com.rmitc.pokemon.model.PokemonTypes;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PokemonService {

    private final PokeApiClient pokeApiClient;
    private final ConversionService conversionService;

    PokemonDto getPokemon(String name) {
        return ofNullable(pokeApiClient.getPokemon(name))
                   .map(pokemon -> conversionService.convert(pokemon, PokemonDto.class))
                   .map(this::populatePokemonWeakness)
                   .map(this::populateTopPokemonWeakness)
                   .orElseThrow(PokemonServiceException::new);
    }

    PokemonTypeDamageDto getPokemonTypeDamage(String type) {
        return ofNullable(pokeApiClient.getPokemonTypeDamage(type))
                   .map(pokemonTypeDamage -> conversionService.convert(pokemonTypeDamage, PokemonTypeDamageDto.class))
                   .orElseThrow(PokemonServiceException::new);
    }

    private PokemonDto populateTopPokemonWeakness(PokemonDto pokemonDto) {
        List<String> topWeakness = new ArrayList<>();
        Map<Double, List<String>> rankings = new TreeMap<>();
        pokemonDto.getWeakness().forEach((key, value) -> {
            List<String> types = rankings.getOrDefault(value, new ArrayList<>());
            types.add(key);
            rankings.put(value, types);
        });
        LinkedList<Double> keys = new LinkedList<>(rankings.keySet());
        Iterator<Double> iterator = keys.descendingIterator();
        while (iterator.hasNext()) {
            topWeakness.addAll(rankings.get(iterator.next()));
            break;
        }
        return pokemonDto.withTopWeakness(topWeakness);
    }

    private PokemonDto populatePokemonWeakness(PokemonDto pokemonDto) {
        Map<String, Double> weakness = initializePokemonWeakness();
        pokemonDto.getTypes().stream().map(this::getPokemonTypeDamage).forEach(pokemonTypeDamage -> updatePokemonWeakness(pokemonTypeDamage, weakness));
        return pokemonDto.withWeakness(weakness);
    }

    private void updatePokemonWeakness(PokemonTypeDamageDto pokemonTypeDamage, Map<String, Double> weakness) {
        updatePokemonWeakness(pokemonTypeDamage.getNoDamageFrom(), Immune, weakness);
        updatePokemonWeakness(pokemonTypeDamage.getHalfDamageFrom(), Ineffective, weakness);
        updatePokemonWeakness(pokemonTypeDamage.getDoubleDamageFrom(), Effective, weakness);
    }

    private void updatePokemonWeakness(List<String> types, PokemonTypeEffectiveness pokemonTypeEffectiveness, Map<String, Double> weakness) {
        types.forEach(type -> weakness.put(type, multiplyUsingBigDecimals(weakness.getOrDefault(type, Neutral.getValue()), pokemonTypeEffectiveness.getValue())));
    }

    private Double multiplyUsingBigDecimals(Double value1, Double value2) {
        return new BigDecimal(String.valueOf(value1)).multiply(new BigDecimal(String.valueOf(value2))).doubleValue();
    }

    private Map<String, Double> initializePokemonWeakness() {
        Map<String, Double> weakness = new HashMap<>();
        EnumSet.allOf(PokemonTypes.class).forEach(pokemonType -> weakness.put(pokemonType.name(), 1.0));
        return weakness;
    }

}
