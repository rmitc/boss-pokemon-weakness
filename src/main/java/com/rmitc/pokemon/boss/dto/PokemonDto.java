package com.rmitc.pokemon.boss.dto;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.Wither;

@Value
@Builder
public class PokemonDto {

    String name;

    List<String> types;

    @JsonProperty("front_default")
    String frontDefault;

    @JsonProperty("front_shiny")
    String frontShiny;

    @JsonProperty("back_default")
    String backDefault;

    @JsonProperty("back_shiny")
    String backShiny;

    @Wither
    Map<String, Double> weakness;

    @Wither
    @JsonProperty("top_weakness")
    List<String> topWeakness;

}
