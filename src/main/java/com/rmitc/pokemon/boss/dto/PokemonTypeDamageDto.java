package com.rmitc.pokemon.boss.dto;

import java.util.List;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class PokemonTypeDamageDto {

    String name;

    List<String> noDamageFrom;

    List<String> halfDamageFrom;

    List<String> doubleDamageFrom;

}
