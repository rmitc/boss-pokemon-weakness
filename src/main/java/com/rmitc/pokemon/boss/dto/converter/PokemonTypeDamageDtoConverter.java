package com.rmitc.pokemon.boss.dto.converter;

import static java.util.stream.Collectors.toList;

import java.util.List;

import com.rmitc.pokemon.boss.dto.PokemonTypeDamageDto;
import com.rmitc.pokemon.model.PokemonType;
import com.rmitc.pokemon.model.PokemonTypeDamage;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class PokemonTypeDamageDtoConverter implements Converter<PokemonTypeDamage, PokemonTypeDamageDto> {

    @Override
    public PokemonTypeDamageDto convert(PokemonTypeDamage pokemonTypeDamage) {
        return PokemonTypeDamageDto.builder()
                   .name(pokemonTypeDamage.getName())
                   .noDamageFrom(toListOfStrings(pokemonTypeDamage.getDamageRelations().getNoDamageFrom()))
                   .halfDamageFrom(toListOfStrings(pokemonTypeDamage.getDamageRelations().getHalfDamageFrom()))
                   .doubleDamageFrom(toListOfStrings(pokemonTypeDamage.getDamageRelations().getDoubleDamageFrom()))
                   .build();
    }

    private List<String> toListOfStrings(List<PokemonType> pokemonTypes) {
        return pokemonTypes.stream().map(PokemonType::getName).collect(toList());
    }

}
