package com.rmitc.pokemon.boss.dto.converter;

import static java.util.stream.Collectors.toList;

import com.rmitc.pokemon.boss.dto.PokemonDto;
import com.rmitc.pokemon.model.Pokemon;
import com.rmitc.pokemon.model.PokemonType;
import com.rmitc.pokemon.model.PokemonTypeSlot;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class PokemonDtoConverter implements Converter<Pokemon, PokemonDto> {

    @Override
    public PokemonDto convert(Pokemon pokemon) {
        return PokemonDto.builder()
                   .name(pokemon.getName())
                   .types(pokemon.getTypes().stream().map(PokemonTypeSlot::getType).map(PokemonType::getName).collect(toList()))
                   .backDefault(pokemon.getSprites().getBackDefault())
                   .backShiny(pokemon.getSprites().getBackShiny())
                   .frontDefault(pokemon.getSprites().getFrontDefault())
                   .frontShiny(pokemon.getSprites().getFrontShiny())
                   .build();
    }

}
