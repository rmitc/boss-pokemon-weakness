package com.rmitc.pokemon.boss;

import java.time.Instant;

import lombok.Builder;
import lombok.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/server/info")
public class ServerResource {

    @GetMapping
    public ServerInfo getServerInfo() {
        return ServerInfo.builder()
                   .time(Instant.now())
                   .os(String.format("%s %s (%s)", System.getProperty("os.name"), System.getProperty("os.version"), System.getProperty("os.arch")))
                   .build();
    }

    @Value
    @Builder
    public static class ServerInfo {

        Instant time;

        String os;

    }

}
