package com.rmitc.pokemon.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class PokemonTypeDamage {

    String name;

    @JsonProperty("damage_relations")
    PokemonTypeDamageRelations damageRelations;

}
