package com.rmitc.pokemon.model;

import java.util.List;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Pokemon {

    String name;

    List<PokemonTypeSlot> types;

    PokemonSprites sprites;

}
