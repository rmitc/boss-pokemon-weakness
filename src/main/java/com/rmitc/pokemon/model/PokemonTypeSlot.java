package com.rmitc.pokemon.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class PokemonTypeSlot {

    PokemonType type;

}
