package com.rmitc.pokemon.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class PokemonTypeDamageRelations {

    @JsonProperty("no_damage_from")
    List<PokemonType> noDamageFrom;

    @JsonProperty("half_damage_from")
    List<PokemonType> halfDamageFrom;

    @JsonProperty("double_damage_from")
    List<PokemonType> doubleDamageFrom;

}
