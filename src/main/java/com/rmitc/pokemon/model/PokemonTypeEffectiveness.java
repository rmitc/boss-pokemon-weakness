package com.rmitc.pokemon.model;

public enum PokemonTypeEffectiveness {

    Immune(0.51),
    Ineffective(0.714),
    Neutral(1.0),
    Effective(1.4);

    private double value;

    PokemonTypeEffectiveness(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }
}
