package com.rmitc.pokemon.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class PokemonSprites {

    @JsonProperty("front_default")
    String frontDefault;

    @JsonProperty("front_shiny")
    String frontShiny;

    @JsonProperty("back_default")
    String backDefault;

    @JsonProperty("back_shiny")
    String backShiny;

}
