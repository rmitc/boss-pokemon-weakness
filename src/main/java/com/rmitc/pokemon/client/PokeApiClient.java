package com.rmitc.pokemon.client;

import com.rmitc.pokemon.model.Pokemon;
import com.rmitc.pokemon.model.PokemonTypeDamage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Component public class PokeApiClient {

    @Value("${pokemonapi.baseUrl}") private String baseUrl;

    private RestTemplate restTemplate;

    @Autowired public void initialize(RestTemplateBuilder restTemplateBuilder) {
        restTemplate = restTemplateBuilder.build();
    }

    public Pokemon getPokemon(String name) {
        try {
            return restTemplate.getForObject(baseUrl + "/pokemon/" + name, Pokemon.class);
        } catch (HttpClientErrorException e) {
            if (HttpStatus.NOT_FOUND.equals(e.getStatusCode())) {
                return null;
            }
            throw e;
        }
    }

    public PokemonTypeDamage getPokemonTypeDamage(String type) {
        try {
            return restTemplate.getForObject(baseUrl + "/type/" + type, PokemonTypeDamage.class);
        } catch (HttpClientErrorException e) {
            if (HttpStatus.NOT_FOUND.equals(e.getStatusCode())) {
                return null;
            }
            throw e;
        }
    }

}
