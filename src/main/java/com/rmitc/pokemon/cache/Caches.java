package com.rmitc.pokemon.cache;

public class Caches {

    public static final String CACHE_KEY_BOSS_POKEMON = "boss_pokemon";

    public static final String CACHE_KEY_POKEMON_TYPE_DAMAGE = "pokemon_type_damage";

}
